#[macro_use]
extern crate clap;
extern crate vfs;

use std::env;
use std::path::{ PathBuf, Path };
use clap::App;
use vfs::{OperationList};

fn main() {
    let yaml = load_yaml!("cli.yml");
    let matches = App::from_yaml(yaml).get_matches();
    let cwd: PathBuf = env::current_dir().unwrap();
    let default_file_path = env::home_dir().unwrap().join(".dc_operations");
    let operations_file_path = matches
        .value_of("operations")
        .unwrap_or(default_file_path.to_str().unwrap());

    let mut operations = OperationList::with_path(cwd.join(Path::new(operations_file_path)).as_path()).unwrap();
    if let Some(matches) = matches.subcommand_matches("ls") {
        let path = matches.value_of("path").unwrap();
        vfs::ls(&operations,Path::new(path))
            .and_then(|vnodes| {
                for node in vnodes {
                    println!("{}", node.as_path().to_str().unwrap());
                }
                Ok(())
            });
    }

//    if let Some(matches) = matches.subcommand_matches("cp") {
//        let src = matches.value_of("source").unwrap();
//        let dst = matches.value_of("destination").unwrap();
//        vfs::cp(&mut operations, Path::new(src), Path::new(dst));
//        println!("Operation CP ajoutée");
//    }
//
//    if let Some(matches) = matches.subcommand_matches("cp") {
//        let src = matches.value_of("source").unwrap();
//        let dst = matches.value_of("destination").unwrap();
//        vfs::mv(&mut operations, Path::new(src), Path::new(dst));
//        println!("Operation MV ajoutée");
//    }
}
