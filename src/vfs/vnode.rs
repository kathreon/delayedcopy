use std::path::{PathBuf, Path};
use std::fs::ReadDir;
use std::io::{Error, ErrorKind};
use VNode;


impl Clone for VNode {
    fn clone(&self) -> VNode {
        VNode { path: self.to_path_buf() }
    }
}

impl VNode {
    pub fn new(path: &Path) -> VNode {
        VNode { path: path.to_path_buf() }
    }

    pub fn list(&self) -> Result<Vec<VNode>, Error>{
        if self.path.is_file() {
            return Err(Error::from(ErrorKind::NotFound));
        }
        self.path.read_dir()
            .and_then(|results: ReadDir| {
                results
                    .map(|result| {
                        match result {
                            Ok(dir_entry) => Ok(VNode::new(&dir_entry.path())),
                            Err(err) => Err(err)
                        }
                    })
                    .collect()
            } )
    }

    pub fn to_path_buf(&self) -> PathBuf {
        self.path.clone()
    }

    pub fn as_path(&self) -> &Path {
        self.path.as_path()
    }
}
