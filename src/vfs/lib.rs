extern crate json;

mod operation;
pub mod path;
mod vnode;

use std::fs::{ ReadDir };
use std::path::{ Path, PathBuf };
use std::io::Error;

#[derive(Debug)]
pub enum Operation {
    Copy {src: String, dst: String},//Should operations have data on source files ? for display ? Should it be Copy{ src: String, vnode: VNode }
    Delete(String)
}

pub struct OperationList {
    operations: Vec<Operation>
}

#[derive(Debug)]
pub struct VNode {
    path: PathBuf
}



pub fn apply(operations: &OperationList) {
    unimplemented!();
}

pub fn exists(operations: &OperationList, path: &Path) -> Option<VNode> {
    for vnode in ls(
        &operations.clone(),
        path.parent().unwrap()
    ).unwrap().iter() {
        if vnode.as_path() == path {
            return Some(vnode.clone());
        }
    }
    return None;
}

pub fn ls(operations: &OperationList, path: &Path) -> Result<Vec<VNode>, Error> {
    let node: VNode = VNode::new(path);
    node.list()
        .and_then(|nodes| {
            operations.virtualize(nodes)
        })
}

pub fn mkdir() {
    unimplemented!();
}
//
//pub fn cp(operations: &mut OperationList, src: &Path, dst: &Path) {
//    ls(&operations.clone(), path)
//        .and_then(|nodes| {
//            for node in nodes.iter() {
//                operations.add(Operation::Delete(
//                    String::from(node.as_path().to_str().unwrap())
//                ));
//                //TODO : recurse over node.list()
//            }
//            Ok(())
//        })
//        .unwrap();
//    operations.add(Operation::Copy {
//        src: String::from(src.to_str().unwrap()),
//        dst: String::from(dst.to_str().unwrap()),
//    });
//
//    if src.is_dir() {
//
//    }
//
//    if dst.is_dir() {
//
//    }
//}
//
//pub fn rm(operations: &mut OperationList, path: &Path) {
//    ls(&operations.clone(), path)
//        .and_then(|nodes| {
//            for node in nodes.iter() {
//                operations.add(Operation::Delete(
//                    String::from(node.as_path().to_str().unwrap())
//                ));
//                //TODO : recurse over node.list()
//            }
//            Ok(())
//        })
//        .unwrap();
//
//    if let Some(vnode) = exists(&operations.clone(), path) {
//        operations.add(Operation::Delete(
//            String::from(path.to_str().unwrap())
//        ));
//    }
//}

//pub fn mv(operations: &mut OperationList, src: &Path, dst: &Path) {
//    cp(operations, src, dst);
//    rm(operations, src);
//}


