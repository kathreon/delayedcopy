use json;
use json::{JsonValue, JsonError};
use std::io::prelude::*;
use std::fs::File;
use std::path::{ PathBuf, Path };
use Operation;
use OperationList;
use VNode;
use std::io::Error;
use path;

impl Operation {
    pub fn get_target_path(&self) -> PathBuf {
        match self {
            &Operation::Copy{ref dst, ref src} => PathBuf::from(dst),
            &Operation::Delete(ref path) => PathBuf::from(path)
        }
    }

    pub fn from_json(op: JsonValue) -> Option<Operation> {
        let op_len = op.len();
        match op[0].as_str().unwrap() {
            "Copy" if (op_len <= 3) => Some(
                Operation::Copy{ src: op[1].to_string(), dst: op[2].to_string() }
            ),
            "Delete" if (op_len <= 2) => Some(
                Operation::Delete(op[1].to_string())
            ),
            _ => None
        }
    }
}

impl Clone for Operation {
    fn clone(&self) -> Operation {
        match self {
            &Operation::Copy{ref dst, ref src} => Operation::Copy {dst: dst.clone(), src: src.clone()},
            &Operation::Delete(ref path) => Operation::Delete(path.clone()),
        }
    }
}

impl OperationList {
    pub fn new(operations: Vec<Operation>) -> OperationList {
        OperationList { operations }
    }

    pub fn with_path(path: &Path) -> Result<OperationList, String> {
        File::open(path::normalize(path))
            .map_err(|err| err.to_string())
            .and_then(|mut file| {
                let mut raw_json = String::new();
                file.read_to_string(&mut raw_json)
                    .map_err(|err| err.to_string())
                    .map(|_| raw_json)
            })
            .and_then(|raw_json|  OperationList::with_json(&raw_json))
    }

    pub fn with_json(raw_json: &String) -> Result<OperationList, String> {
        json::parse(&raw_json)
            .map_err(|err| err.to_string())
            .and_then(|value | {
                match value {
                    json::JsonValue::Array(raw_operations) => {
                        let mut operations = Vec::new();
                        for op in raw_operations {
                            if let Some(operation) = Operation::from_json(op) {
                                operations.push(operation);
                            }
                        }
                        Ok(operations)
                    },
                    _ => Err("Invalid json".to_string())
                }
            })
            .and_then(|operations| {
                Ok(OperationList::new(operations))
            })
    }

    pub fn add(&mut self, operation: Operation) {
        self.operations.push(operation);
    }

    pub fn virtualize(&self, nodes: Vec<VNode>) -> Result<Vec<VNode>, Error> {
        let mut entries : Vec<VNode> = self.virtual_entries();
        entries.extend(nodes);
        Ok(self.filter_virtual_entries(entries))
    }

    pub fn last_operation(&self, path: PathBuf) -> Option<&Operation> {
        for op in self.operations.iter().rev() {
            if op.get_target_path() == path {
                return Some(&op);
            }
        }
        None
    }

    fn virtual_entries(&self) -> Vec<VNode> {
        let mut entries : Vec<VNode> = Vec::new();
        for op in self.operations.iter().rev() {
            match *op {
                Operation::Copy{src: _, dst: ref op_path} => {
                    if let Some(&Operation::Copy{ref src, ref dst}) = self.last_operation(PathBuf::from(op_path)) {
                        entries.push(VNode::new(&PathBuf::from(op_path)));
                    }
                }
                _ => continue
            }
        }
        entries
    }

    fn filter_virtual_entries(&self, entries: Vec<VNode> ) -> Vec<VNode> {
        let mut vnodes = entries.clone();
        for node in entries {
            match self.last_operation(node.to_path_buf()) {
                Some(&Operation::Delete(ref op_path)) => {
                    if let Some(index) = vnodes.iter()
                        .position(|node| node.to_path_buf() == PathBuf::from(op_path)) {
                        vnodes.remove(index);
                    }
                },
                _ => continue
            }
        }
        vnodes
    }

    fn optimize(&self) {
        unimplemented!();
    }
}


impl Clone for OperationList {
    fn clone(&self) -> OperationList {
        let mut vec = Vec::new();
        for operation in self.operations.iter() {
            vec.push(operation.clone());
        }
        OperationList { operations: vec }
    }
}
