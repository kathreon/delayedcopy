# Delayed Copy

DELETE
COPY

 dc -o ~/.dc_operations cp src dst
 dc -o ~/.dc_operations mv src dst
 dc -o ~/.dc_operations rm path
 dc -o ~/.dc_operations ls path
 dc -o ~/.dc_operations tree path
 dc -o ~/.dc_operations apply

TODO :

global : ensure path normalization
mv / cp / rm : add & compose operations ( with path normalization )
apply : optimize operations & write operations
tree : recursion over ls & display
ls : proper display & sorting

https://deterministic.space/elegant-apis-in-rust.html
